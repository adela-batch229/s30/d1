db.fruits.insertMany([
  {
    name : "Apple",
    supplier: "Red Farms Inc.",
    stocks : 20,
    price: 40,
    onSale : true,
  },

  {
    name : "Banana",
    supplier : "Yellow Farms",
    stocks : 15,
    price: 20,
    onSale : true,
  },

  {
    name : "Kiwi",
    supplier : "Green Farming and Canning",
    stocks : 25,
    price: 50,
    onSale : true,
  },

  {
    name : "Mango",
    supplier : "Yellow Farms",
    stocks : 10,
    price: 60,
    onSale : true,
  },
  {
    name : "Dragon Fruit",
    supplier: "Red Farms Inc.",
    stocks : 10,
    price: 60,
    onSale : true,
  },

])

//Aggregation
//Aggregation in MongoDb is typically done 2-3 steps. Each process/step in aggregation 
// is called a stage
db.fruits.aggregate([
    // Aggregate documents to get the total stocks of fruits per supplier.
    
    //$match - used to match or get documents that satisfies the condition.
    //syntaxL {$match:{field:<value>}}
    {$match:{onSale:true}}, //apple,dragon fruit, banna, mango, kiwi

    //$group - allows us to group together documents and create an analysis out of the group documents

    //_id: in the group stage, essentially assosciates an id to our results.
    //_id: also determines the number of groups.

    //_id: "$supplier" - essentially grouped together documents with the same values in the supplier field.
    //_id: "$<field>" - groups documents based on the value of the indicated field.

    /*
        $ match- apple, kiwi, banana, dragon fruit, mango
        $group - _id - $supplier
        $sum - used to add or total values in a given field
        $sum: $stocks - we got the sum of the values of the stock field of each item per group

        $sum: "$<field>" - sums the values of the given field per group

        _id: Red Farms
        apple 
        supplier - Red Farms
        stocks : 20
        dragon fruit
        supplier - Red Farms
        stocks: 10
        $sum: 30        

        _id: Green Farming
        kiwi 
        supplier - Green Farming
        stocks: 25
        $sum: 25

        _id: Yellow Farm
        banana
        supplier - Yellow Farms
        stocks: 15
        mango
        supplier - Yellow Farms
        stocks: 10
        $sum: 25
    */
    {$group:{_id:"$supplier", totalStocks:{$sum:"$stocks"}}}])

//What if we add the value of _id as null?
//if the _id's value is definite or given, $group will only create one group
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:null, totalStocks:{$sum:"$stocks"}}}

])//will give 1 group of onsale and their totalstocks

//if the _id's value is definite or given, $group will only create one group
db.fruits.aggregate([
    {$match:{onSale:true}},
    {$group:{_id:"AllFruits", totalStocks:{$sum:"$stocks"}}}

])// will find 1 group named allfruits

db.fruits.aggregate([
    //$match stage is similar to find(). In fact, you can even add every query operators
       // to expand the criteria
    {$match:{$and:[ {onSale:true},{price:{$gte:40}} ] }},
    {$group:{_id:"$supplier", totalStocks:{$sum:"$stocks"}}}
])//will find the total stocks of each supplier that has price greater than 40

db.fruits.aggregate([
    //$match stage is similar to find(). In fact, you can even add every query operators
       // to expand the criteria
    //mango,kiwi,apple
    {$match:{$and:[ {onSale:true},{price:{$gte:40}} ] }},
    {$group:{_id:"$supplier", totalStocks:{$sum:"$stocks"}}}
])

//$avg - is an operator used in $group stage
//$avg - gets the average of the values of the given field per group.


//gets the average stock of fruits per supplier for all item on sale
db.fruits.aggregate([

    {$match:{onSale:true}},
    {$group:{_id:"$supplier", avgStock: {$avg: "$stocks"}}}
    
]

db.fruits.aggregate([

    //Apple, kiwi, mango,banana
    {$match: {onSale:true}},
    {$group: {_id:null, avgPrice:{$avg:"$price"}}}

])//gets the average price of all fruits on sale.

db.fruits.aggregate([
     
    //Apple,Dragonfruit
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"group1", avgPrice:{$avg:"$price"}}}

])//gets the average price of all fruits which supplier is Red Farms Inc.

//$max - will allow us to get the highes value of out all the values in 
// a given field per group.

db.fruits.aggregate([

       {$match:{onSale:true}},
       {$group: {_id:"maxStockOnSale", maxStock:{$max:"$stocks"}}}

])// gets the highest number of stock of all fruits on sale

db.fruits.aggregate([

       {$match:{onSale:true}},
       {$group: {_id:"highestPrice", maxPrice:{$max:"$price"}}}

])//gets the highest price of all fruits on sale

//$min - gets the lowest value of the given field per group
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"lowestStockOnSale", minStock:{$min:"$stocks"}}}

])//gets the lowest number of stock of all items on sale


db.fruits.aggregate([

    {$match: {supplier:"Yellow Farms"}},
    {$group: {_id:"lowestPriceYellowFarms", minPrice:{$min:"$price"}}}

])//gets the fruit with the minimum price on yellow farms supplier

//$count - is a stage usually added after $match to count all items that 
//matches our criteria
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$count: "itemsOnSale"}

])// Count all items on sale

db.fruits.aggregate([

    {$match: {price:{$gt:50}}},
    {$count: "itemsPriceMoreThan50"}

])//gets the number of items that its price has more than 50

db.fruits.aggregate([

    {$match: {$and: [ {price:{$gte:50}}, {stocks: {$lt:20}}]}},
    {$count: "itemsPriceGreaterThan50Below20Stocks"}

])//gets the number of items that price is greater than 50 and stock is less than 20

//$out - save/outputs the results in a new collection.
//it is usually the last stage in an aggregation pipeline.
//note: this will overwrite the collection if it already exists.

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}},
    {$out: "stocksPerSupplier"}

])

db.fruits.aggregate([

    {$match: {price:{$lte:50}}},
    {$group: {_id:"$supplier", totalStocks:{$sum:"$stocks"}}},
    {$out: "stocksPerSupplier"}

]) //overwrites stocksPersupplier

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier", avgPrice:{$avg:"$price"}}},
    {$out: "avgPricePerSupplier"}

])//creates another collection named avgPricePerSupplier with the avg price of fruits on sale 

//Mini-Activity
//Use mongodb aggregation to get the lowest number of stockk per supplier for all items onsale
//then save the results in a collection called lowesNumberOfStock
//screenshot the resulting collection and send it in our hangouts.

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier", minStock:{$min:"$stocks"}}},
    {$out: "lowesNumberOfStock"}

])